//
//  CZPageControl.swift
//  3.UIPageControl
//
//  Created by CZ on 2020/11/24.
//

import UIKit

class CZPageControl: UIPageControl {
    
    var currentImage: UIImage?
    var inactiveImage: UIImage?
    var currentTintColor: UIColor?
    var inactiveTintColor: UIColor?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = false
        if #available(iOS 14.0, *) {
            self.allowsContinuousInteraction = false
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var currentPage: Int {
        didSet {
            updateDots()
        }
    }
    
    func updateDots() {
        guard let currentImage = self.currentImage, let inactiveImage = self.inactiveImage else {
            return
        }
        
        if #available(iOS 14.0, *) {
            guard let dotContentView = findIndicatorContentView() else {
                return
            }
            
            for (index, view) in dotContentView.subviews.enumerated() {
                if view.isKind(of: UIImageView.self) {
                    self.currentPageIndicatorTintColor = self.currentTintColor
                    self.pageIndicatorTintColor = self.inactiveTintColor
                    
                    let indicatorView = view as! UIImageView
                    indicatorView.image = nil
                    if index == self.currentPage {
                        indicatorView.image = currentImage.withRenderingMode(.alwaysTemplate)
                    } else {
                        indicatorView.image = inactiveImage.withRenderingMode(.alwaysTemplate)
                    }
                }
            }
        } else {
            for (index, view) in self.subviews.enumerated() {
                if let dot = imageViewForSubview(view, currentPage: index) {
                    var size = CGSize.zero
                    
                    if index == self.currentPage {
                        dot.tintColor = self.currentTintColor
                        dot.image = currentImage.withRenderingMode(.alwaysTemplate)
                        size = dot.image!.size
                    } else {
                        dot.tintColor = self.inactiveTintColor
                        dot.image = inactiveImage.withRenderingMode(.alwaysTemplate)
                        size = dot.image!.size
                    }
                    
                    if let superview = dot.superview {
                        let x = (superview.frame.size.width - size.width) / 2.0
                        let y = (superview.frame.size.height - size.height) / 2.0
                        dot.frame = CGRect(x: x, y: y, width: size.width, height: size.height)
                    }
                }
            }
        }
    }
    
    // iOS 14之前创建UIImageView使用
    func imageViewForSubview(_ view: UIView, currentPage: Int) -> UIImageView? {
        
        var dot: UIImageView?
        if view.isKind(of: UIView.self) {
            for subview in view.subviews {
                if subview.isKind(of: UIImageView.self) {
                    dot = (subview as! UIImageView)
                    break
                }
            }
            
            if dot == nil {
                dot = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
                view.addSubview(dot!)
            }
        } else {
            dot = (view as! UIImageView)
        }
        
        return dot
    }
    
    @available(iOS 14.0, *)
    func findIndicatorContentView() -> UIView? {
        for contentView in self.subviews {
            if let contentViewClass = NSClassFromString("_UIPageControlContentView"), contentView.isKind(of: contentViewClass) {
                for indicatorContentView in contentView.subviews {
                    if let indicatorContentViewClass = NSClassFromString("_UIPageControlIndicatorContentView"), indicatorContentView.isKind(of: indicatorContentViewClass) {
                        return indicatorContentView
                    }
                }
            }
        }
        return nil
    }
}

