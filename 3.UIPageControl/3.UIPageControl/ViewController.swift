//
//  ViewController.swift
//  3.UIPageControl
//
//  Created by CZ on 2020/11/24.
//

import UIKit

class ViewController: UIViewController {

    let customControl = CZPageControl()
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        preferredIndicatorImage()
//        indicatorImageForPage()
        customPageControl()
    }

    func preferredIndicatorImage() {
        let control = UIPageControl()
        control.numberOfPages = 10
        control.frame = CGRect(x: 0, y: 300, width: 350, height: 30)
        control.pageIndicatorTintColor = .systemRed
        control.currentPageIndicatorTintColor = .systemGreen
        if #available(iOS 14.0, *) {
            control.preferredIndicatorImage = UIImage(named:"heart")
        }
        self.view.addSubview(control)
    }
    
    func indicatorImageForPage() {
        let indicatorImages = ["summy", "cloudy", "rainy", "thunder", ""]
        let control = UIPageControl()
        control.numberOfPages = indicatorImages.count
        control.frame = CGRect(x: 0, y: 300, width: 350, height: 30)
        control.pageIndicatorTintColor = .systemGray
        control.currentPageIndicatorTintColor = .systemTeal
        self.view.addSubview(control)
        if #available(iOS 14.0, *) {
            for (idx, imageName) in indicatorImages.enumerated() {
                control.setIndicatorImage(UIImage(named:imageName), forPage: idx)
            }
        }
    }

    func customPageControl() {
        customControl.frame = CGRect(x: 0, y: 100, width: 350, height: 30)
        customControl.numberOfPages = 10
        customControl.currentTintColor = .systemTeal
        customControl.inactiveTintColor = .systemGray4
        customControl.currentImage = UIImage(named: "dot_current")
        customControl.inactiveImage = UIImage(named: "dot_inactive")
        self.view.addSubview(customControl)
        
        customControl.currentPage = 0
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var page = customControl.currentPage + 1
        if page >= customControl.numberOfPages {
            page = 0
        }
        customControl.currentPage = page
    }
}





