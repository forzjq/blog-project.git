//
//  CZDecorationView.swift
//  7.DecorationView
//
//  Created by CZ on 2020/12/8.
//

import UIKit

class CZDecorationView: UICollectionReusableView {

    var imageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(imageView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        self.imageView.frame = layoutAttributes.bounds
        let attributes = layoutAttributes as! CZDecorationAttributes
        if let name = attributes.imageName {
            self.imageView.image = UIImage(named: name)
        }
    }
}
