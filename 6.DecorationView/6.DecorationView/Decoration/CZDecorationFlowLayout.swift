//
//  CZDecorationFlowLayout.swift
//  7.DecorationView
//
//  Created by CZ on 2020/12/8.
//

import UIKit
protocol CZDecorationFlowLayoutDelegate: UICollectionViewDelegateFlowLayout {
    
    func layout(_ layout: CZDecorationFlowLayout, decorationImageAt section: Int) -> String
    
    func layout(_ layout: CZDecorationFlowLayout, decorationImageAt indexPath: IndexPath) -> String
    
}


class CZDecorationFlowLayout: UICollectionViewFlowLayout {
    let decorationViewKind = "CZDecorationView"
    var itemsAttribute = [UICollectionViewLayoutAttributes]()
    weak var cz_delegate: CZDecorationFlowLayoutDelegate?
    
    override func prepare() {
        super.prepare()
        self.register(CZDecorationView.self, forDecorationViewOfKind: decorationViewKind)
        
        self.itemsAttribute.removeAll()
        let sections = self.collectionView?.numberOfSections ?? 0
        
        // 给section添加一张背景图片
//        for i in 0..<sections {
//            let attribute = CZDecorationAttributes.init(forDecorationViewOfKind: decorationViewKind, with: IndexPath(item: 0, section: i))
//            attribute.zIndex = -1
//            attribute.imageName = self.cz_delegate?.layout(self, decorationImageAt: i)
//
//            if let count = self.collectionView?.numberOfItems(inSection: i), count > 0 {
//                let firstItem = self.layoutAttributesForItem(at: IndexPath(item: 0, section: i))
//                let lastItem = self.layoutAttributesForItem(at: IndexPath(item: (count - 1), section: i))
//                let height = lastItem!.frame.maxY - firstItem!.frame.minY
//                attribute.frame = CGRect(x: 0, y: firstItem!.frame.minY, width: self.collectionView!.frame.size.width, height: height)
//                self.itemsAttribute.append(attribute)
//            }
//        }
        
        // 给cell添加背景图片
//        for i in 0..<sections {
//            let itemCount = self.collectionView?.numberOfItems(inSection: i) ?? 0
//            for item in 0..<itemCount {
//                let indexPath = IndexPath(item: item, section: i)
//                let attribute = CZDecorationAttributes.init(forDecorationViewOfKind: decorationViewKind, with: indexPath)
//                attribute.zIndex = -1
//                attribute.imageName = self.cz_delegate?.layout(self, decorationImageAt: indexPath)
//                let itemAttributes = self.layoutAttributesForItem(at: indexPath)!
//                attribute.frame = itemAttributes.frame
//                self.itemsAttribute.append(attribute)
//            }
//        }
        
        // 给collectionView可滚动范围添加背景图片
        if let itemCountInSection = self.collectionView?.numberOfItems(inSection: (sections - 1)), itemCountInSection > 0 {
            let firstItem = self.layoutAttributesForItem(at: IndexPath(item: 0, section: 0))
            let lastItem = self.layoutAttributesForItem(at: IndexPath(item: (itemCountInSection - 1), section: (sections - 1)))
            let indexPath = IndexPath(item: 0, section: 0)
            let attribute = CZDecorationAttributes.init(forDecorationViewOfKind: decorationViewKind, with: indexPath)
            attribute.zIndex = -1
            attribute.imageName = "6"
            let height = lastItem!.frame.maxY - firstItem!.frame.minY
            attribute.frame = CGRect(x: 0, y: 0, width: self.collectionView!.frame.size.width, height: height)
            self.itemsAttribute.append(attribute)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributes = super.layoutAttributesForElements(in: rect)
        
        for attribute in self.itemsAttribute {
            if rect.intersects(attribute.frame) {
                attributes?.append(attribute)
            }
        }
        return attributes
    }
}

