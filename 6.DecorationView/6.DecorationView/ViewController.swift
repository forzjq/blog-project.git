//
//  ViewController.swift
//  6.DecorationView
//
//  Created by CZ on 2020/12/11.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, CZDecorationFlowLayoutDelegate {

    

    @IBOutlet weak var collectionView: UICollectionView!
    
    var images = ["0", "1", "2", "3", "4", "5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = self.collectionView.collectionViewLayout as! CZDecorationFlowLayout
        layout.cz_delegate = self
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return images.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath)
        cell.contentView.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 50, right: 10)
    }
    
    func layout(_ layout: CZDecorationFlowLayout, decorationImageAt section: Int) -> String {
        return images[section]
    }
    
    func layout(_ layout: CZDecorationFlowLayout, decorationImageAt indexPath: IndexPath) -> String {
        return images[indexPath.section]
    }
}

