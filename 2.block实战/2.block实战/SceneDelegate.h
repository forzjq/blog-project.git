//
//  SceneDelegate.h
//  2.block实战
//
//  Created by CZ on 2020/11/23.
//  Copyright © 2020 CZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

