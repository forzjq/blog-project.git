//
//  ViewController.swift
//  1.发送短信
//
//  Created by CZ on 2020/11/23.
//  Copyright © 2020 CZ. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController {

    let number: String = "13123456789"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func openURLMethod(_ sender: Any) {
        UIApplication.shared.open(URL(string: "sms://\(number)")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func messageUIMethod(_ sender: Any) {
        if MFMessageComposeViewController.canSendText() {
            let vc = MFMessageComposeViewController()
            vc.recipients = [number] // 支持多个手机号
            vc.body = "今天晚上有空么,一起吃个饭" // 支持文字直接进入文本框
            vc.messageComposeDelegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension ViewController: MFMessageComposeViewControllerDelegate {
    
    /// 发送短信回调
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil);
        
        switch result {
        case .cancelled:
            print("短信发送 -- 取消")
        case .sent:
            print("短信发送 -- 成功")
        case .failed:
            print("短信发送-- 失败")
        default:
            break
        }
    }
    
    
}

