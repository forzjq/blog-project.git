//
//  ViewController.m
//  4.颜色差异
//
//  Created by CZ on 2020/11/25.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *ibView;
@property (weak, nonatomic) IBOutlet UILabel *codeView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.codeView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:85.0/255.0 blue:34.0/255.0 alpha:1.0];
    
    // 创建GenericRGB,与设备无关;Interface Builder中默认值
    [UIColor colorWithCGColor:CGColorCreateGenericRGB(255.0/255.0, 85.0/255.0, 34.0/255.0, 1.0)];
    
    // Apple RGB 等同于 sRGB
    [UIColor colorWithRed:255.0/255.0 green:85.0/255.0 blue:34.0/255.0 alpha:1.0];
    
    // sRGB
    [UIColor colorWithCGColor:CGColorCreateSRGB(255.0/255.0, 85.0/255.0, 34.0/255.0, 1.0)];
    
    // Diplay P3
    [UIColor colorWithDisplayP3Red:255.0/255.0 green:85.0/255.0 blue:34.0/255.0 alpha:1.0];
    
    // Adobe RGB
    CGColorSpaceRef space = CGColorSpaceCreateWithName(kCGColorSpaceAdobeRGB1998);
    CGFloat components[] = {255.0/255.0, 85.0/255.0, 34.0/255.0, 1.0};
    CGColorRef calibratedRGBColorRef = CGColorCreate(space, components);
    [UIColor colorWithCGColor:calibratedRGBColorRef];
}


@end
